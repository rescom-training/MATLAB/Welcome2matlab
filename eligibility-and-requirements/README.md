# Eligibility and Requirements

## What do you need to know before you come?

**Participants must bring a laptop with access to MATLAB (the newest version if possible) and a charger.**

To install MATLAB using the university license, please follow the instructions [here](https://github.com/resbaz/lessons/blob/master/matlab/unimelb_matlab_install.md), or [here](https://studentit.unimelb.edu.au/software). For technical issues and support you can contact the [Student IT Centre](https://studentit.unimelb.edu.au/) or click [here](https://online.unimelb.edu.au/support/current-students/apps-and-software-info/systems-and-support). To use MATLAB from home or off campus, you need to install a VPN to 'tunnel' into the campus network. You can find more information [here](https://studentit.unimelb.edu.au/wireless-vpn/vpn).

Alternatively (in case you can't install MATLAB on your laptop, or use it remotely), you can use the online version of MATLAB by following step 1 and 2 listed [here](https://github.com/resbaz/lessons/blob/master/matlab/unimelb_matlab_install.md). You can find more information about the MATLAB online version [here](https://au.mathworks.com/products/matlab-online.html). To upload files from desktop to the cloud, please go to [MATLAB Drive](https://drive.matlab.com).

The trainings starts on a beginner’s level (no prior knowledge of MATLAB is required) and moves to data processing, statistics and plotting on an intermediate level (prior knowledge of MATLAB is required; Note: Not suitable for advanced programmers, except you want to repeat the concepts listed in [Training overview](overview.md)).

As mentioned above, you don’t need to have any previous knowledge of the tools that will be presented at the workshop. However, if you have no previous programming experience, we recommend that you consult the course materials for [introduction to programming concepts](https://nikkirubinstein.gitbooks.io/resguides-introductory-programming-concepts/content/content/welcome-to-coding.html) prior to attending the current workshop.

**Other helpful resources are:**\
Websites for serious programming or math/numerical questions:\
https://stackoverflow.com/ \
https://math.stackexchange.com/ 

An overview of MathWorks facilities for academia: https://www.mathworks.com/academia.html

Distance Learning community (MathWorks is building this out now) - https://www.mathworks.com/matlabcentral/topics/distance-learning.html \
Websites for interaction with other MATLAB users: https://au.mathworks.com/matlabcentral/answers/ \
Blogs - https://blogs.mathworks.com/ \
Cody (is a MATLAB problem-solving game) - https://www.mathworks.com/matlabcentral/cody \
File exchange - https://au.mathworks.com/matlabcentral/fileexchange/ 

**Self-Paced Online Courses -** https://matlabacademy.mathworks.com/ \
MATLAB Documentation - https://au.mathworks.com/help/index.html 

Formal certification program -  https://www.mathworks.com/services/training/certification.html \
Full training catalog - https://www.mathworks.com/services/training.html \
Using MATLAB for research in public clouds - https://www.mathworks.com/solutions/cloud.html

Wikibook for MATLAB Beginners - https://en.wikibooks.org/wiki/MATLAB_Programming


**My trainings are accessible, engaging and encouraging, as I’m passionate about giving other researchers the ability to help themselves. Be curios, motivated, and willing to participate and communicate with other students/researchers in an open, generous and respectful way.**

So,

* Be prepared and motivated,
* Participate and communicate,
* Be respectful, and share your knowledge (if applicable).

## Please bring with you:

* [ ] a laptop with internet access and a web browser;
* [ ] a laptop charger;
* [ ] and if you are attending one of our Zoom workshops, you must download Zoom. Instructions on how to do this are provided here: [https://lms.unimelb.edu.au/students/student-guides/zoom](https://lms.unimelb.edu.au/students/student-guides/zoom).

---

Continue to [Expectations](expectations.md), or (re)visit one of the other pages listed [here](../SUMMARY.md).

