# MATLAB Modules

**For information and updates from the** _**University of Melbourne**_ **regarding COVID-19, please refer to:**

http://www.unimelb.edu.au/coronavirus



Welcome to MATLAB modules, presented to you by _Research Computing Services_. We offer workshops and community events for researchers throughout the year to teach the valuable digital tool skills needed to take your research to the next level. We are researchers, training researchers!

For more information about _Research Computing Services_, please visit: https://research.unimelb.edu.au/infrastructure/research-computing-services

**If you would like to access the MATLAB modules, please login with unimelb credentials or send an email to alicia.resplat@gmail.com to request access.**

You can sign up to attend our training and community events on Eventbrite: http://rescomunimelb.eventbrite.com

## What next?

To find out more about this training, and to decide whether this is for you, please read the following information:

* [About this training](overview.md)
* [About the trainer](trainer.md)
* [Training format](training-format/README.md)
    * [Dates and times](training-format/dates-and-times.md)
* [Eligibility and requirements](eligibility-and-requirements/README.md)
    * [Expectations](eligibility-and-requirements/expectations.md)
    * [Support](eligibility-and-requirements/support.md)

