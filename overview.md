# Training overview

* What is MATLAB and what can it do for you?

MATLAB is a numerical computing environment and programming language that was designed to be interactive, intuitive and able to deal with large datasets. Because of its lineage, MATLAB is mostly used in science and engineering communities to do things such as matrix manipulations. How is this useful? Many datasets come as matrices that can be manipulated in MATLAB. For example, large Excel spreadsheets and image files can be converted into matrices. MATLAB allows you to work with these matrices efficiently and effectively.
There has recently been growing interest from researchers across other faculties (Arts, Economics, Medicine and Psychology) who want to use MATLAB for data analysis and visulatisation.

Watch the short YouTube-video with the title "MATLAB - What choice will you make?" to gain more insight ([click here](https://www.youtube.com/watch?v=iqv3kV-uQNM)).
<!-- blank line -->
<figure class="video_container">
  <iframe width="1280" height="720" src="https://www.youtube.com/embed/iqv3kV-uQNM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
<!-- blank line -->

* What will be covered in MATLAB trainings?

The MATLAB modules will teach you how to write codes in MATLAB for (Big) Data Processiong, Statistical Analysis, Plotting, Documentations, and High-Performance Computing.

## Learning objectives

This training aims to enable students to write codes in MATLAB for data processiong, analysis and visualisation. The modules will concentrate on following objectives: 

* [ ] Simple coding in MATLAB using scripts
* [ ] Familiarity with data types in MATLAB
* [ ] Indexing 
* [ ] Logical operators
* [ ] Applying if-statements and for-loops
* [ ] Writing functions
* [ ] Working with tabular data in MATLAB
* [ ] Categorising (tabular) data in MATLAB
* [ ] Working around missing data in MATLAB
* [ ] Functions and visualisation techniques useful in (descriptive) statistics
* [ ] Curve fitting and trend analysis
* [ ] Discovering Interpolation methods
* [ ] Understanding Graphics Object Hierarchy (GOH)
* [ ] Using GOH and Objects Handles to customise plots
* [ ] Learning optimum ways to save plots for reports and publications
* [ ] Compatibility of MATLAB and LaTex
* [ ] Working with NetCDF files in MATLAB
* [ ] Using the Spartan system for High-Performance Computing 



## Learning outcomes

* Analytical and problem-solving skills: By working on challenges
* Programming and computational skils: By following the training and provided resources 
* Collaborative and communication skills: By participating in groupworks and providing help to your peers once you have learned the basics

---

Continue to [About the trainer](trainer.md), or (re)visit one of the other pages listed [here](SUMMARY.md).

