# Training Format

## The current format

Whilst we are practicing social distancing, we will be hosting Zoom sessions. These sessions will include a mini-training or pitch, followed by a meet-up (see descriptions below).

* Duration: one to two hours.
* Mode: online, using Zoom.
* Frequency: weekly.

## The pitch

A short 30 minute session introducing you to MATLAB. The pitch provides an opportunity to gain some basic skills, try out the tool, and engage in a team challenge.

* Duration: 30 minutes.
* Mode: typically in-person.
* Frequency: typically the pitch is presented bi-annually at key events such as Researcher Connect, and Graduate Researcher orientation days.

## The (mini) training

A full introductory or intermediate workshop.

* Duration: 1 hours (online); 3-4 hours face-to-face.
* Mode: blended, including in-person workshop supplemented by online content.
* Frequency: weekly (online); monthly (face-to-face).

## The meet-up

An opportunity to ask questions, learn some tips and tricks, see some exemplars and get to know the community that is actively using MATLAB. Also known as a data lab, hacky hour or drop-in.

* Duration: one to two hours.
* Mode: typically in-person.
* Frequency: occasionally (online); monthly (face-to-face).

---

Continue to [Dates and times](../training-format/dates-and-times.md), or (re)visit one of the other pages listed [here](../SUMMARY.md).

